//
//  HeaderView.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 12/07/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit

public class HeaderView: UICollectionReusableView {
	
	public static var nib = UINib(nibName: "HeaderView", bundle: nil);
	
	public static var reuseIdentifier = "headerView";
	
	public static var supplementaryViewKind = "headerView";
	
	@IBOutlet weak var titleLabel: UILabel!;
	
	@IBOutlet weak var toolbar: UIToolbar!;
	
	public var title: String? {
		get {
			return titleLabel.text;
		}
		set {
			titleLabel.text = newValue;
		}
	}
	
	public var titleColor: UIColor {
		get {
			return titleLabel.textColor;
		}
		set {
			titleLabel.textColor = newValue;
		}
	}
	
	public func setBarButtonItems(_ buttonItems: [UIBarButtonItem], animated: Bool = false) {
		toolbar.setItems(buttonItems, animated: animated);
	}
	
}
