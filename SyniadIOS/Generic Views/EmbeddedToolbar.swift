//
//  EmbeddedToolbar.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 20/09/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit

@IBDesignable
class EmbeddedToolbar: UIToolbar {
	
	override init(frame: CGRect) {
		super.init(frame: frame);
		setTransparency();
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder);
		setTransparency();
	}

	public func setTransparency() {
		setBackgroundImage(UIImage(), forToolbarPosition: .any, barMetrics: .default);
		setShadowImage(UIImage(), forToolbarPosition: .any);
	}

}
