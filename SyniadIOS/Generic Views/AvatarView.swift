//
//  AvatarView.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 26/02/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import SyniadClient
import SyniadShared
import Foundation
import UIKit

// A view for displaying an avatar. Essentially an imageView that automatically masks itself, and has a convenience loader.
@IBDesignable
public class AvatarView: UIImageView {
	
	public override var frame: CGRect {
		didSet {
			debugPrint(frame);
			self.layer.cornerRadius = frame.size.width / 2;
		}
	}
	
	public override init(frame: CGRect) {
		super.init(frame: frame);
		self.clipsToBounds = true;
		loadDefault();
	}
	
	public required init?(coder: NSCoder) {
		super.init(coder: coder);
		self.clipsToBounds = true;
		loadDefault();
	}
	
	
	
}

public extension UIImageView {
	
	public func loadPlaceholder() {
		self.image = nil;
	}
	
	public func load(from id: ID?) {
		guard let id = id else {
			loadDefault();
			return;
		}
		imageLoader.getImage(for: id).onMain({ (image) in
			self.image = image;
		}).whenFailure { (error) in
			debugPrint(error.self);
		}
	}
	
	public func loadDefault() {
		self.image = UIImage(named: "default-avatar");
	}
	
}
