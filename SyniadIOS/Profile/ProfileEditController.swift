//
//  ProfileEditController.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 28/02/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit
import SyniadShared
import SyniadClient
import SyniadViewModel
import NIO

public class ProfileEditController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
	
	@IBOutlet weak var avatarImage: AvatarView!
	
	@IBOutlet weak var usernameField: UITextField!
	
	@IBOutlet weak var bioTextView: UITextView!
	
	@IBOutlet weak var selectImageButton: UIButton!
	
	@IBOutlet weak var doneButton: UIButton!
	
	var bioDelegate: PlaceholderTextViewDelegate!;
	
	public var viewModel: ProfileEditViewModel? {
		didSet {
			setup();
		}
	}
	
	var cancellables = Set<AnyCancellable>();
	
	public override func viewDidLoad() {
		// Connect the editing actions on the username field to the validate function to prevent the user from setting an empty username.
		bioDelegate = PlaceholderTextViewDelegate(displaying: "Add a Bio...", for: bioTextView);
		bioDelegate.onChange(onBioUpdated(_:));
		setup();
	}
	
	func setup() {
		guard isViewLoaded, let viewModel = viewModel else {
			return;
		}
		viewModel.defaultUsername.onMain( { [unowned self] (username) in
			usernameField.text = username;
		}, store: &cancellables);
		viewModel.defaultDescription.onMain({ [unowned self] (description) in
			bioTextView.text = description;
		}, store: &cancellables);
		viewModel.defaultAvatarID.onMain({ [unowned self] (id) in
			avatarImage.load(from: id);
		}, store: &cancellables);
	}
	
	@IBAction func usernameUpdate(_ sender: Any) {
		viewModel?.username = usernameField.text;
	}
	
	func onBioUpdated(_ textView: UITextView) {
		viewModel?.description = bioTextView.text;
	}
	
	@IBAction func selectImage(_ sender: Any) {
		let picker = UIImagePickerController();
		picker.delegate = self;
		picker.popoverPresentationController?.sourceRect = selectImageButton.frame;
		self.present(picker, animated: true, completion: nil);
	}
	
	public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		picker.dismiss(animated: true, completion: nil);
	}
	
	public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		picker.dismiss(animated: true, completion: nil);
		let selectedImage = info[.originalImage] as! UIImage;
		self.avatarImage.image = selectedImage;
		viewModel?.avatarData = selectedImage.jpegData(compressionQuality: 0.5);
	}
	
	@IBAction func cancel(_ sender: Any) {
		self.dismiss(animated: true, completion: nil);
	}
	
	@IBAction func done(_ sender: Any) {
		viewModel?.execute().onMain({ [unowned self] (_) in
			dismiss(animated: true, completion: nil);
		})
	}
	
}
