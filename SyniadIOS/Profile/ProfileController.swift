//
//  ProfileController.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 27/02/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit
import SyniadShared
import SyniadClient
import SyniadViewModel

public enum ProfileCellItem: Hashable, Equatable {
	case profile
	case item(Item)
}

typealias ProfileSnapshot = NSDiffableDataSourceSnapshot<ProfileDisplaySection, ProfileCellItem>;

let profileItemHeaderKind = "profileItemSectionLayoutKind";

typealias ProfileDataSource = UICollectionViewDiffableDataSource<ProfileDisplaySection, ProfileCellItem>;

// A controller for managing the currently logged in profile.
public class ProfileController: UICollectionViewController {
	
	private var dataSource: ProfileDataSource?;
	
	private var snapshot: ProfileSnapshot?;
	
	public var viewModel: ProfileDisplayViewModel? {
		didSet {
			setup();
		}
	}
	
	var cancellables = Set<AnyCancellable>();
	
	public override func viewDidLoad() {
		// Retrieve the profile from the network
		collectionView.register(ItemCell.nib, forCellWithReuseIdentifier: ItemCell.reuseIdentifier);
		collectionView.register(ProfileDetailCell.nib, forCellWithReuseIdentifier: ProfileDetailCell.reuseIdentifier);
		collectionView.register(HeaderView.nib, forSupplementaryViewOfKind: HeaderView.supplementaryViewKind, withReuseIdentifier: HeaderView.reuseIdentifier);
		collectionView.setCollectionViewLayout(generateLayout(), animated: false);
		dataSource = ProfileDataSource(collectionView: collectionView, cellProvider: { [unowned self] (collectionView, path, cellItem) -> UICollectionViewCell? in
			switch cellItem {
			case .profile:
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileDetailCell.reuseIdentifier, for: path) as! ProfileDetailCell;
				viewModel?.username.onMain({ (username) in
					cell.usernameLabel.text = username;
				}, store: &cancellables);
				viewModel?.description.onMain({ (description) in
					cell.descriptionText.text = description;
				}, store: &cancellables);
				viewModel?.avatarID.onMain({ (id) in
					cell.avatarView.load(from: id);
				}, store: &cancellables);
				cell.toolbar.items = profileBarItems();
				return cell;
			case .item(let item):
				let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCell.reuseIdentifier, for: path) as! ItemCell;
				itemCell.viewModel = ItemCellViewModel(for: item, implicitUser: viewModel?.id);
				return itemCell;
			}
		});
		dataSource?.supplementaryViewProvider = { [unowned self] (collectionView, kind, indexPath) -> UICollectionReusableView? in
			let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: HeaderView.supplementaryViewKind, withReuseIdentifier: HeaderView.reuseIdentifier, for: indexPath) as! HeaderView;
			switch viewModel?.sections[indexPath.section] {
			case .published:
				headerView.title = "Uploads";
			case .liked:
				headerView.title = "Liked";
			default:
				break;
			}
			let expandItem = UIBarButtonItem(title: "Show All", style: .plain, target: self, action: #selector(self.showAll(sender:)));
			expandItem.tag = indexPath.section;
			headerView.setBarButtonItems([
				UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
				expandItem
			])
			return headerView;
		};
		collectionView.dataSource = dataSource;
		setup();
	}
	
	public func profileBarItems() -> [UIBarButtonItem] {
		return [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)]
	}
	
	public func setup() {
		guard isViewLoaded, let viewModel = viewModel else {
			return;
		}
		
		snapshot = ProfileSnapshot();
		
		viewModel.$sections.onMain( { [unowned self] (sections) in
			guard snapshot != nil else {
				return;
			}
			snapshot!.deleteSections(snapshot!.sectionIdentifiers);
			snapshot!.appendSections(sections);
			snapshot!.appendItems([.profile], toSection: .profileDetail);
			for section in sections {
				guard section != .profileDetail else {
					break;
				}
				snapshot!.appendItems(viewModel.items(for: section).map({ (item) -> ProfileCellItem in
					return .item(item);
				}), toSection: section);
			}
			dataSource?.apply(snapshot!);
		}, store: &cancellables);
		
		viewModel.itemsLoadedEvent.onMain({ [unowned self] (data) in
			let (section, items) = data;
			guard snapshot != nil, snapshot!.sectionIdentifiers.contains(section) else {
				return;
			}
			snapshot?.clearSection(section);
			snapshot?.appendItems(items.map({ (item) -> ProfileCellItem in
				return .item(item);
			}), toSection: section);
			dataSource?.apply(snapshot!);
		}, store: &cancellables);
	}
	
	@objc func showAll(sender: UIBarButtonItem) {
		
	}
	
	func generateLayout() -> UICollectionViewLayout {
		return UICollectionViewCompositionalLayout { (index, _) -> NSCollectionLayoutSection? in
			if self.hideSection(at: index) {
				return generateEmptySection();
			}
			switch index {
			case 0:
				return self.generateProfileLayout();
			default:
				return self.generateItemLayout();
			}
		}
	}
	
	func generateProfileLayout() -> NSCollectionLayoutSection {
		let profileLayoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(128));
		let profileLayoutItem = NSCollectionLayoutItem(layoutSize: profileLayoutSize);
		let profileGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(128));
		let profileGroup = NSCollectionLayoutGroup.vertical(layoutSize: profileGroupSize, subitem: profileLayoutItem, count: 1);
		profileGroup.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: .none, top: .fixed(8), trailing: .none, bottom: .fixed(8));
		profileGroup.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 8, bottom: 0, trailing: 8);
		let section = NSCollectionLayoutSection(group: profileGroup);
		return section;
	}
	
	/// Generates the layout for standard horizontally scrolling item view displays.
	func generateItemLayout() -> NSCollectionLayoutSection {
		// Configure the horizontally scrollable content section.
		let itemLayoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(1));
		let itemLayoutItem = NSCollectionLayoutItem(layoutSize: itemLayoutSize);
		let itemGroupSize = NSCollectionLayoutSize(widthDimension: .absolute(300), heightDimension: .absolute(300));
		let itemGroup = NSCollectionLayoutGroup.horizontal(layoutSize: itemGroupSize, subitem: itemLayoutItem, count: 1);
		itemGroup.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4);
		let section = NSCollectionLayoutSection(group: itemGroup);
		section.orthogonalScrollingBehavior = .continuous;
		// Configure the section header.
		let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(44));
		let headerItem = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: HeaderView.supplementaryViewKind, alignment: .top);
		section.boundarySupplementaryItems = [headerItem];
		return section;
	}
	
	func hideSection(at index: Int) -> Bool {
		guard let viewModel = viewModel else {
			return true;
		}
		if let snapshot = snapshot {
			return snapshot.numberOfItems(inSection: viewModel.sections[index]) == 0;
		} else {
			return false;
		}
	}

	public var selectedItem: Item?;
	
	public override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
		guard let viewModel = viewModel else {
			return false;
		}
		
		return viewModel.sections[indexPath.section] != .profileDetail;
	}
	
	public override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let viewModel = viewModel else {
			return;
		}
		
		let section = viewModel.sections[indexPath.section];
		let items = viewModel.items(for: section);
		selectedItem = items[indexPath.item];
	}
	
	public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let itemDetailController = segue.destination as? ItemDetailController {
			guard let item = selectedItem else {
				return;
			}
			itemDetailController.viewModel = ItemDetailViewModel(for: item);
		}
	}
	
}

public class UserProfileController: ProfileController {
	
	public override func viewDidLoad() {
		if let profileId = client.profile {
			viewModel = ProfileDisplayViewModel(for: profileId);
			print(profileId);
		}
		
		super.viewDidLoad();
	}

	public override func profileBarItems() -> [UIBarButtonItem] {
		let uploadItem = UIBarButtonItem(image: UIImage(systemName: "arrow.up.doc"), style: .plain, target: self, action: #selector(onCreateItem));
		let editProfileItem = UIBarButtonItem(image: UIImage(systemName: "square.and.pencil"), style: .plain, target: self, action: #selector(onEdit));
		return [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), uploadItem, editProfileItem];
	}
	
	@objc public func onEdit() {
		// Ensure the profile is loaded, since the editor depends on the profile.
		guard (viewModel?.profile != nil) ?? false else {
			return;
		}
		performSegue(withIdentifier: "editProfile", sender: self);
	}
	
	@objc public func onCreateItem() {
		performSegue(withIdentifier: "createItem", sender: self);
	}
	
	public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender);
		if let navigationController = segue.destination as? UINavigationController, let destination = navigationController.topViewController as? ItemEditController {
			destination.viewModel = viewModel?.itemUploadViewModel;
		}
		if let destination = segue.destination as? ProfileEditController {
			destination.viewModel = viewModel?.profileEditViewModel;
		}
	}
	
}

