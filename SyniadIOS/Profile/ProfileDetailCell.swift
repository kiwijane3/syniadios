//
//  ProfileDetailCell.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 6/07/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadShared
import SyniadViewModel

class ProfileDetailCell: CombineCollectionViewCell {
	
	public static let nib = UINib(nibName: "ProfileDetailCell", bundle: nil);
	
	public static let reuseIdentifier = "profileDetailCell";
	
	@IBOutlet weak var avatarView: AvatarView!;
	
	@IBOutlet weak var usernameLabel: UILabel!;
	
	@IBOutlet weak var toolbar: UIToolbar!
	
	@IBOutlet weak var descriptionText: UITextView!
	
}
