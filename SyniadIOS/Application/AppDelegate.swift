//
//  AppDelegate.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 20/02/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadClient
import SyniadShared
import SyniadViewModel
import NIO

internal var serverURL = "app.luoja.co";

internal var imageLoader: ImageLoader!;

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		debugPrint("Launching");
		client = SyniadClient(toServer: serverURL);
		let window = application.windows.first;
		imageLoader = ImageLoader(using: client);
		// Load server resources.
		initialiseClient(locale: Locale.current.languageCode ?? "en").flatMap { (_) -> EventLoopFuture<Void> in
			return client.treeController.load();
		}.flatMap { (_) -> EventLoopFuture<Bool> in
			// Check if loginInfo is available
			return client.accountController.use(loginInfo: retrieveLoginInfo());
		}.onMain { (loggedIn) in
			if loggedIn {
				self.loadMain();
			} else {
				self.loadLogin();
			}
		}.catchOnMain { (error) in
			errorAlert(for: error, in: window?.rootViewController);
		}
		return true
	}

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}

	func loadLogin() {
		let storyboard = UIStoryboard(name: "Main", bundle: nil);
		let loginController = storyboard.instantiateViewController(identifier: "loginRoot");
		UIApplication.shared.windows.first?.rootViewController = loginController;
		debugPrint("Loaded login");
	}

	func loadMain() {
		let storyboard = UIStoryboard(name: "Main", bundle: nil);
		let mainController = storyboard.instantiateViewController(identifier: "mainRoot");
		UIApplication.shared.windows.first?.rootViewController = mainController;
		debugPrint("Loaded main");
	}
	
}

// Returns a succeeded future to allow for conditional asynchronous code to be chained off of.
internal func beginAsync() -> EventLoopFuture<Void> {
	return client.eventLoop.makeSucceededFuture(Void());
}

internal func localise(_ identifier: String?) -> String? {
	guard let identifier = identifier else {
		return nil;
	}
	return client.localisationController.localised(for: identifier);
}
