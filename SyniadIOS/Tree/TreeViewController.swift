//
//  TreeViewController.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 3/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadViewModel
import SyniadShared
import SyniadClient
import Combine

public enum TreeSection {
	case path
	case children
}

class TreeViewController: UICollectionViewController {
	
	internal var snapshot: NSDiffableDataSourceSnapshot<TreeSection, String>?;
	
	internal var dataSource: UICollectionViewDiffableDataSource<TreeSection, String>?;
	
	public var viewModel: TreeSelectorViewModel?;
	
	internal var sections: [TreeSection] = [.path, .children];
	
	var cancellables = Set<AnyCancellable>();
	
	public override func viewDidLoad() {
		collectionView.register(TreeCell.nib, forCellWithReuseIdentifier: TreeCell.identifier);
		collectionView.register(HeaderView.nib, forSupplementaryViewOfKind: HeaderView.supplementaryViewKind, withReuseIdentifier: HeaderView.supplementaryViewKind);
		collectionView.setCollectionViewLayout(generateLayout(), animated: false);
		dataSource = UICollectionViewDiffableDataSource(collectionView: collectionView, cellProvider: { (collectionView, indexPath, identifier) -> UICollectionViewCell? in
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TreeCell.identifier, for: indexPath) as! TreeCell;
			cell.identifier = identifier;
			return cell;
		});
		dataSource?.supplementaryViewProvider = { (collectionView, kind, indexPath) -> UICollectionReusableView in
			let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: HeaderView.supplementaryViewKind, withReuseIdentifier: HeaderView.reuseIdentifier, for: indexPath) as! HeaderView;
			switch self.sections[indexPath.section] {
			// These are placeholders, add something more meaningful before release.
			case .path:
				headerView.title = "Path";
			case .children:
				headerView.title = "Children";
			}
			return headerView;
		}
		collectionView.dataSource = dataSource;
		setup();
	}
	
	public func setup() {
		guard isViewLoaded, let viewModel = viewModel else {
			return;
		}
		snapshot = NSDiffableDataSourceSnapshot<TreeSection, String>();
		snapshot?.appendSections([.path, .children]);
		viewModel.$path.sink { (path) in
			self.update(section: .path, to: path);
		}.store(in: &cancellables);
		viewModel.$children.sink { (children) in
			self.update(section: .children, to: children);
		}.store(in: &cancellables);
	}
	
	public func update(section: TreeSection, to contents: [String]) {
		guard snapshot != nil else {
			return
		}
		snapshot?.clearSection(section);
		snapshot?.appendItems(contents, toSection: section);
		dataSource?.apply(snapshot!);
	}
	
	public func generateLayout() -> UICollectionViewCompositionalLayout {
		return UICollectionViewCompositionalLayout { (index, _) -> NSCollectionLayoutSection? in
			if self.hideSection(at: index) {
				return generateEmptySection();
			}
			switch self.sections[index] {
			case .path:
				return self.generatePathLayout();
			case .children:
				return self.generateChildrenLayout();
			}
		}
	}
	
	public func generatePathLayout() -> NSCollectionLayoutSection {
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(0.125));
		let item = NSCollectionLayoutItem(layoutSize: itemSize);
		item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4);
		let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(0.125));
		let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 1);
		let section = NSCollectionLayoutSection(group: group);
		let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(44));
		let headerItem = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: HeaderView.supplementaryViewKind, alignment: .top);
		section.boundarySupplementaryItems = [headerItem];
		return section;
	}
	
	public func generateChildrenLayout() -> NSCollectionLayoutSection {
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.5), heightDimension: .fractionalWidth(0.125));
		let item = NSCollectionLayoutItem(layoutSize: itemSize);
		item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4);
		let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(0.125));
		let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2);
		let section = NSCollectionLayoutSection(group: group);
		let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(44));
		let headerItem = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: HeaderView.supplementaryViewKind, alignment: .top);
		section.boundarySupplementaryItems = [headerItem];
		return section;
	}
	
	public func hideSection(at index: Int) -> Bool {
		if let snapshot = snapshot {
			return snapshot.numberOfItems(inSection: sections[index]) == 0;
		} else {
			return false;
		}
	}
	
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let viewModel = viewModel else {
			return
		}
		switch sections[indexPath.section] {
		case .path:
			viewModel.ascend(to: indexPath.item);
		case .children:
			viewModel.descend(to: indexPath.item);
		}
	}
}
