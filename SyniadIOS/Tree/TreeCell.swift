//
//  TreeCell.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 3/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadClient
import SyniadViewModel

class TreeCell: UICollectionViewCell {
	
	public static let nib = UINib(nibName: "TreeCell", bundle: nil);
	
	public static let identifier = "treeCell";
	
	@IBOutlet weak var image: UIImageView!;
	
	@IBOutlet weak var label: UILabel!;
	
	@IBOutlet weak var labelContainer: UIView!;
	
	public var identifier: String? {
		didSet {
			reload();
		}
	}
	
	public override func awakeFromNib() {
		super.awakeFromNib();
		contentView.layer.cornerRadius = 8;
		contentView.clipsToBounds = true;
		labelContainer.clipsToBounds = true;
		labelContainer.layer.cornerRadius = 8;
	}
	
	public func reload() {
		if let identifier = identifier {
			label.text = client.localisationController.localised(for: identifier);
			// image.load(from: "\(identifier)_image");
			image.loadDefault();
		}
	}
	
}
