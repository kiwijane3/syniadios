//
//  ItemDetailView.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 14/09/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadShared
import SyniadViewModel

public class ItemDetailController: UIViewController {
	
	// MARK:- Views
	
	@IBOutlet weak var titleLabel: UILabel!
	
	@IBOutlet weak var mainToolbar: EmbeddedToolbar!
	
	@IBOutlet weak var imageView: UICollectionView!
	
	@IBOutlet weak var creatorAvatar: AvatarView!
	
	@IBOutlet weak var createNameLabel: UILabel!
	
	@IBOutlet weak var creatorToolbar: EmbeddedToolbar!
	
	@IBOutlet weak var categoryLabel: UILabel!
	
	@IBOutlet weak var styleLabel: UILabel!
	
	@IBOutlet weak var descriptionView: UITextView!
	
	@IBOutlet weak var tagView: UICollectionView!
	
	// MARK:- Adapters
	
	public var tagAdapter: TagCollectionAdapter?;
	
	public var galleryAdapter: GalleryDisplayAdapter?;
	
	// MARK:- View Model
	
	public var viewModel: ItemDetailViewModel?;
	
	var cancellables = Set<AnyCancellable>();
	
    public override func viewDidLoad() {
		super.viewDidLoad();
		
		tagAdapter = TagCollectionAdapter();
		tagAdapter?.attach(to: tagView);
		
		galleryAdapter = GalleryDisplayAdapter();
		galleryAdapter?.attach(to: imageView);
		
		setup();
    }
	
	public func setup() {
		guard isViewLoaded, let viewModel = viewModel else {
			return;
		}
		
		viewModel.title.onMain({(title) in
			self.titleLabel.text = title;
		}, store: &cancellables);
		viewModel.categoryText.onMain({ (categoryText) in
			self.categoryLabel.text = categoryText;
		}, store: &cancellables);
		viewModel.styleText.onMain({ (styleText) in
			self.styleLabel.text = styleText;
		}, store: &cancellables);
		viewModel.description.onMain({ (description) in
			self.descriptionView.text = description;
		}, store: &cancellables);
		
		viewModel.images.onMain({ (images) in
			self.galleryAdapter?.images = images;
		}, store: &cancellables);
		viewModel.tags.onMain({ (tags) in
			self.tagAdapter?.tags = tags;
		}, store: &cancellables);
		
		viewModel.username.onMain({ (username) in
			self.createNameLabel.text = username;
		}, store: &cancellables);
		viewModel.avatarID.onMain( { (id) in
			self.creatorAvatar.load(from: id);
		}, store: &cancellables);
		
	}
	
}
