//
//  TagCollectionAdapter.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 13/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import SyniadShared
import SyniadViewModel
import UIKit

public typealias TagAdapterDataSource = UICollectionViewDiffableDataSource<Int, TagDisplayItem>;

public typealias TagAdapterSnapshot = NSDiffableDataSourceSnapshot<Int, TagDisplayItem>;

public class TagCollectionAdapter {
	
	public var tags: [TagDisplayItem] = [] {
		didSet {
			load();
		}
	}
	
	public var collectionView: UICollectionView?;
	
	public var dataSource: TagAdapterDataSource?;
	
	// We use a constraint to set the height of the tag collection to a calculated value, because determing an intrinsic size for a collection view does not appear to be possible.
	public var heightConstraint: NSLayoutConstraint?;
	
	public func attach(to collectionView: UICollectionView) {
		self.collectionView = collectionView;
		collectionView.register(TagCell.nib, forCellWithReuseIdentifier: TagCell.reuseIdentifier);
		dataSource = TagAdapterDataSource(collectionView: collectionView, cellProvider: { (collectionView, indexPath, item) -> UICollectionViewCell? in
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TagCell.reuseIdentifier, for: indexPath) as! TagCell;
			switch item {
			case .tag(let tag):
				cell.text = tag;
			case .placeholder:
				cell.text = "No Tags";
			}
			return cell;
		});
		collectionView.dataSource = dataSource;
		collectionView.setCollectionViewLayout(generateLayout(), animated: false);
		// Setup the collectionView to have a size calculated to fit the content.
		heightConstraint = collectionView.heightAnchor.constraint(equalToConstant: 0);
		collectionView.superview?.addConstraint(heightConstraint!);
		load();
	}
	
	public func load() {
		var snapshot = TagAdapterSnapshot();
		snapshot.appendSections([0]);
		snapshot.appendItems(tags, toSection: 0);
		dataSource?.apply(snapshot);
		// Calculate the height of the collectionView to fit the elements.
		heightConstraint?.constant = 44 * CGFloat(numberOfRows());
	}
	
	public func generateLayout() -> UICollectionViewLayout {
		return UICollectionViewCompositionalLayout { (_, _) -> NSCollectionLayoutSection? in
			if self.tags.isEmpty {
				return generateEmptySection();
			} else {
				return self.generateSection();
			}
		}
	}
	
	public func generateSection() -> NSCollectionLayoutSection {
		// We want to have a flowing layout, where the first items appear in the first column and there are no more than 10 columns. This requires the use of a horizontal group consisting of vertical groups with a manually calculated number of rows.
		let rows = numberOfRows();
		let itemSize = NSCollectionLayoutSize(widthDimension: .absolute(128), heightDimension: .absolute(44));
		let item = NSCollectionLayoutItem(layoutSize: itemSize);
		item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4);
		let verticalGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1));
		let verticalGroup = NSCollectionLayoutGroup.vertical(layoutSize: verticalGroupSize, subitem: item, count: rows);
		let horizontalGroupSize = NSCollectionLayoutSize(widthDimension: .absolute(128), heightDimension: .absolute(CGFloat(rows) * 44));
		let horizontalGroup = NSCollectionLayoutGroup.horizontal(layoutSize: horizontalGroupSize, subitems: [verticalGroup]);
		let section = NSCollectionLayoutSection(group: horizontalGroup);
		section.orthogonalScrollingBehavior = .continuous;
		return section;
	}
	
	public func numberOfRows() -> Int {
		return ((tags.count - 1) / 5) + 1;
	}
	
}


