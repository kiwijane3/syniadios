//
//  TagSelectorController.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 11/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadShared
import SyniadViewModel
import SyniadClient
import Combine

private let reuseIdentifier = "Cell"

public typealias TagSelectorDataSource = UICollectionViewDiffableDataSource<TagSelectorDisplaySection, String>;

public typealias TagSelectorDataSourceSnapshot = NSDiffableDataSourceSnapshot<TagSelectorDisplaySection, String>;

public class TagSelectorController: UICollectionViewController, UISearchResultsUpdating, TagSelectorPresenter {

	public var viewModel: TagSelectorViewModel?  {
		didSet {
			setup();
		}
	}
	
	public var dataSource: TagSelectorDataSource?;
	
	public var snapshot: TagSelectorDataSourceSnapshot?;
	
	public var searchController: UISearchController?;
	
	var cancellables = Set<Combine.AnyCancellable>();
	// MARK:- Initialisation
	
	public override func viewDidLoad() {
		navigationItem.title = "Tags";
		let searchController = UISearchController();
		searchController.searchResultsUpdater = self;
		navigationItem.searchController = searchController;
		collectionView.register(TagCell.nib, forCellWithReuseIdentifier: TagCell.reuseIdentifier);
		collectionView.register(HeaderView.nib, forSupplementaryViewOfKind: HeaderView.supplementaryViewKind, withReuseIdentifier: HeaderView.reuseIdentifier);
		dataSource = TagSelectorDataSource(collectionView: collectionView, cellProvider: { (collectionView, indexPath, tag) -> UICollectionViewCell? in
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TagCell.reuseIdentifier, for: indexPath) as! TagCell;
			cell.text = tag;
			return cell;
		})
		dataSource?.supplementaryViewProvider = { (collectionView, kind, indexPath)-> UICollectionReusableView in
			let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: HeaderView.reuseIdentifier, for: indexPath) as! HeaderView;
			switch self.dataSource?.snapshot().sectionIdentifiers[indexPath.section] {
			case .selected:
				view.title = "Selected";
			case .suggestions:
				view.title = "Suggestions";
			case .searchResults:
				view.title = "Results";
			default:
				break;
			}
			return view;
		}
		collectionView.dataSource = dataSource;
		collectionView.setCollectionViewLayout(generateLayout(), animated: false);
		setup();
	}
	
	public func setup() {
		guard isViewLoaded, let viewModel = viewModel else {
			return;
		}
		snapshot = TagSelectorDataSourceSnapshot();
		viewModel.$sections.sink { (sections) in
			self.present(sections: sections);
		}.store(in: &cancellables);
		viewModel.$selectedTags.sink { (selected) in
			self.present(items: selected, in: .selected);
		}.store(in: &cancellables);
		viewModel.$suggestedTags.sink { (suggested) in
			self.present(items: suggested, in: .suggestions);
		}.store(in: &cancellables);
		viewModel.$searchResults.sink { (results) in
			self.present(items: results, in: .searchResults);
		}.store(in: &cancellables);
	}

	public func present(sections: [TagSelectorDisplaySection]) {
		guard snapshot != nil, let viewModel = viewModel else {
			return
		}
		snapshot!.deleteSections(self.snapshot!.sectionIdentifiers);
		snapshot!.deleteAllItems();
		for section in sections {
			snapshot!.appendSections([section]);
			snapshot!.appendItems(viewModel.items(for: section), toSection: section);
		}
		dataSource?.apply(snapshot!);
	}
	
	public func present(items: [Tag], in section: TagSelectorDisplaySection) {
		guard snapshot != nil, let viewModel = viewModel, viewModel.sections.contains(section) else {
			return;
		}
		if snapshot!.sectionIdentifiers.contains(section) {
			snapshot?.clearSection(section);
			snapshot?.appendItems(items, toSection: section);
		}
		dataSource?.apply(snapshot!);
	}
	
	// MARK:- Selection

	public override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
		guard let snapshot = dataSource?.snapshot() else {
			return false;
		}
		switch snapshot.sectionIdentifiers[indexPath.section] {
		case .suggestions, .searchResults:
			return true;
		case .selected:
			return false;
		}
	}
	
	public override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let viewModel = viewModel else {
			return;
		}
		viewModel.select(at: indexPath.item);
	}
	
	public override func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
		guard let viewModel = viewModel else {
			return nil;
		}
		if viewModel.sections[indexPath.section] == .selected {
			return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { (_) -> UIMenu? in
				let delete = UIAction(title: "Delete", image: UIImage(systemName: "trash"), identifier: nil, attributes: [.destructive], handler: { _ in
					viewModel.remove(at: indexPath.item);
				});
				return UIMenu(title: "", children: [delete]);
			}
		} else {
			return nil;
		}
	}
	
	// MARK:- Search
	
	public func updateSearchResults(for searchController: UISearchController) {
		let prefix = searchController.searchBar.text;
		if let prefix = prefix, !prefix.isEmpty {
			viewModel?.search(for: prefix);
		} else {
			viewModel?.endSearch();
		}
	}
	
	// MARK:- Layout
	
	public func generateLayout() -> UICollectionViewCompositionalLayout {
		return UICollectionViewCompositionalLayout { (index, _) -> NSCollectionLayoutSection in
			if self.hideSection(at: index) {
				return generateEmptySection();
			}
			return self.generateSectionLayout();
		}
	}
	
	public func generateSectionLayout() -> NSCollectionLayoutSection {
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.5), heightDimension: .fractionalWidth(0.125));
		let item = NSCollectionLayoutItem(layoutSize: itemSize);
		item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4);
		let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(0.125));
		let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2);
		let section = NSCollectionLayoutSection(group: group);
		let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(44));
		let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: HeaderView.supplementaryViewKind, alignment: .top);
		section.boundarySupplementaryItems = [header];
		return section;
	}
	
	public func hideSection(at index: Int) -> Bool {
		guard let snapshot = dataSource?.snapshot() else {
			return false;
		}
		return snapshot.itemIdentifiers(inSection: snapshot.sectionIdentifiers[index]).isEmpty;
	}
}

public class TagCell: UICollectionViewCell {
	
	public static let reuseIdentifier = "tagCell";
	
	public static let nib = UINib(nibName: "TagCell", bundle: nil);
	
	public var text: String? {
		didSet {
			load();
		}
	}
		
	@IBOutlet weak var label: UILabel!
	
	public override func awakeFromNib() {
		contentView.layer.cornerRadius = 8;
		contentView.clipsToBounds = true;
	}
	
	public func load() {
		label.text = text;
	}
	
}

public protocol TagSelectorDelegate {
	
	func onUpdateTags(to tags: [Tag]);
	
}
