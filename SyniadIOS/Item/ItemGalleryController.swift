//
//  ItemSearchController.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 23/09/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadShared
import SyniadClient
import SyniadViewModel

public typealias ItemSearchDataSource = UICollectionViewDiffableDataSource<SingleSection, Item>;

public typealias ItemSearchSnapshot = NSDiffableDataSourceSnapshot<SingleSection, Item>;

class ItemGalleryController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
	
	// MARK:- Variables
	
	public var dataSource: ItemSearchDataSource?;
	
	public var spacing: CGFloat = 8;
	
	public var minimumCellWidth: CGFloat = 300;
	
	public var widthForCachedSize: CGFloat?;
	
	public var cachedSize: CGSize?;
	
	public var segueTarget: Item?;
	
	public var viewModel: ItemGalleryViewModel? {
		didSet {
			setup();
		}
	}
	
	var cancellables = Set<AnyCancellable>();
	
	// MARK:- Initialisation
	
	public override func viewDidLoad() {
		collectionView.register(ItemCell.nib, forCellWithReuseIdentifier: "itemCell");
		dataSource = ItemSearchDataSource(collectionView: collectionView, cellProvider: { (collectionView, index, item) -> UICollectionViewCell? in
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: index) as! ItemCell;
			cell.viewModel = ItemCellViewModel(for: item);
			return cell;
		});
		collectionView.dataSource = dataSource;
		let layout = UICollectionViewFlowLayout();
		layout.sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing);
		layout.minimumInteritemSpacing = spacing;
		layout.minimumLineSpacing = spacing;
		collectionView.setCollectionViewLayout(layout, animated: false);
		setup();
	}
	
	public func setup() {
		// Create a default viewModel if one is not assigned yet.
		guard isViewLoaded else {
			return;
		}
		if viewModel == nil {
			viewModel = ItemGalleryViewModel();
		}
		viewModel?.$items.receive(on: RunLoop.main).sink(receiveValue: { [unowned self] (items) in
			var snapshot = ItemSearchSnapshot();
			snapshot.appendSections([.only]);
			snapshot.appendItems(items, toSection: .only);
			dataSource?.apply(snapshot);
		}).store(in: &cancellables)
	}
	
	
	// This method is used to begin loading the next part of the content when the user approaches the end of the displayed content.
	public override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if let viewModel = viewModel, indexPath.item < viewModel.items.count - 5 {
			viewModel.fetch();
		}
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		if let widthForCachedSize = widthForCachedSize, let cachedSize = cachedSize, widthForCachedSize == collectionView.frame.width {
			return cachedSize;
		} else {
			widthForCachedSize = collectionView.frame.width;
			// Calculate the total space to display.
			let space = collectionView.frame.width - spacing;
			// Calculate how many cells can fit in the space, rounding down so each cell is at leat the minimum size.
			let cellsPerRow = (space / (minimumCellWidth + spacing)).rounded(.down);
			let availableSpace = space - (cellsPerRow * spacing);
			let cellWidth = availableSpace / cellsPerRow;
			cachedSize = CGSize(width: cellWidth, height: cellWidth);
			return cachedSize!;
		}
	}
	
	// MARK:- Transitions
	
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		viewModel?.select(at: indexPath.item);
		performSegue(withIdentifier: "showItem", sender: self);
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let destination = segue.destination as? ItemDetailController {
			destination.viewModel = viewModel?.selectedItemModel;
		}
		if let navigationController = segue.destination as? UINavigationController, let destination = navigationController.topViewController as? ItemFilterController {
			destination.viewModel = viewModel?.filterModel;
		}
	}
	
}

internal class ItemSearchController: ItemGalleryController, UISearchBarDelegate {
	
	public var searchBar: UISearchBar!;
	
	public override func viewDidLoad() {
		searchBar = UISearchBar();
		searchBar.delegate = self;
		navigationItem.titleView = searchBar;
		super.viewDidLoad();
	}
	
	public func search(using term: String?) {
		guard let viewModel = viewModel else {
			return;
		}
		viewModel.searchTerm = term;
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if searchBar.text == "" || searchBar.text == nil {
			search(using: nil);
		}
	}
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		search(using: searchBar.text);
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		search(using: nil);
	}
	
}
