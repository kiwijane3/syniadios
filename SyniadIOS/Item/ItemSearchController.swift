//
//  ItemSearchController.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 23/09/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadShared
import SyniadClient

public enum ItemSearchContext {
	case publisher(ID)
	case liked(ID)
	case general
}

public enum ItemSearchSection: Hashable {
	case main
}

public enum ItemCellItem: Hashable {
	case item(Item)
}

public typealias ItemSearchDataSource = UICollectionViewDiffableDataSource<ItemSearchSection, ItemCellItem>;

public typealias ItemSearchSnapshot = NSDiffableDataSourceSnapshot<ItemSearchSection, ItemCellItem>;

class ItemGalleryController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

	public var context: ItemSearchContext = .general;
	
	public var dataSource: ItemSearchDataSource?;
	
	public var snapshot: ItemSearchSnapshot?;
	
	public var loader: ItemLoader?;
	
	public var loading: Bool = false;
	
	public override func viewDidLoad() {
		collectionView.register(ItemCell.nib, forCellWithReuseIdentifier: "itemCell");
		dataSource = ItemSearchDataSource(collectionView: collectionView, cellProvider: { (collectionView, index, cellItem) -> UICollectionViewCell? in
			switch cellItem {
			case .item(let item):
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: index) as! ItemCell;
				cell.item = item;
				return cell;
			}
		});
		collectionView.dataSource = dataSource;
		let layout = UICollectionViewFlowLayout();
		layout.itemSize = CGSize(width: 300, height: 300);
		collectionView.setCollectionViewLayout(layout, animated: false);
		setup();
	}
	
	public func setup() {
		snapshot = ItemSearchSnapshot();
		snapshot?.appendSections([.main])
		var filter = SearchFilter();
		switch context {
		case .liked(let liker):
			filter.likedBy = liker;
		case .publisher(let publisher):
			filter.publisher = publisher;
		default:
			break;
		}
		loader = client.itemController.loader(with: filter);
		loadItems();
		update();
	}
	
	public func loadItems() {
		if !loading, let loader = loader, !loader.allLoaded {
			loading = true;
			loader.fetchNext()?.onMain({ (items) in
				let cellItems = items.map { (item) -> ItemCellItem in
					return .item(item);
				}
				self.snapshot?.appendItems(cellItems, toSection: .main);
				self.update();
				self.loading = false;
			})
		}
	}
	
	public func update() {
		if let snapshot = snapshot {
			dataSource?.apply(snapshot);
		}
	}
	
	// This method is used to begin loading the next part of the content when the user approaches the end of the displayed content.
	public override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if !loading, let loader = loader, indexPath.item > loader.endIndex - 5 {
			self.loadItems();
		}
	}
	
}

public class
