//
//  ItemDetailView.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 14/09/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadShared

public enum ItemDetailSection: Hashable {
	case header
	case gallery
	case parameter
	case description
}

public enum ItemDetailItem: Hashable {
	case header
	case gallery(ID)
	case parameter(ItemParameter)
	case description
}

typealias ItemDetailSnapshot = NSDiffableDataSourceSnapshot<ItemDetailSection, ItemDetailItem>;

typealias ItemDetailDataSource = UICollectionViewDiffableDataSource<ItemDetailSection, ItemDetailItem>;

public class ItemDetailController: UICollectionViewController {
	
	public var itemId: ID? {
		didSet {
			if item == nil || item?.id != itemId {
				fetchItem();
			}
		}
	}
	
	public var item: Item? {
		didSet {
			itemId = item?.id;
			load();
		}
	}
	
	public var sections: [ItemDetailSection] = [.header, .gallery, .parameter, .description];
	
	private var snapshot: ItemDetailSnapshot?;
	
	private var dataSource: ItemDetailDataSource?;
	
    public override func viewDidLoad() {
		super.viewDidLoad();
		collectionView.setCollectionViewLayout(generateLayout(), animated: false);
		dataSource = ItemDetailDataSource(collectionView: collectionView, cellProvider: { (collectionView, index, item) -> UICollectionViewCell? in
			switch item {
			case .header:
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "headerCell", for: index) as! ItemHeaderCell;
				cell.item = self.item;
				return cell;
			case .gallery(let imageId):
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: index) as! ItemGalleryCell;
				cell.imageID = imageId;
				return cell;
			case .parameter(let parameter):
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "parameterCell", for: index) as! ItemParameterCell;
				cell.parameter = parameter;
				cell.item = self.item;
				return cell;
			case .description:
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "descriptionCell", for: index) as! ItemDescriptionCell;
				cell.item = self.item;
				return cell;
			}
		});
		collectionView.dataSource = dataSource;
		setup();
    }
	
	public func setup() {
		snapshot = ItemDetailSnapshot();
		snapshot?.appendSections(sections);
		load();
	}
	
	public func load() {
		if var snapshot  = snapshot {
			let galleryItems = item?.images.map({ (id) -> ItemDetailItem in
				return .gallery(id);
			}) ?? [];
			snapshot.setItems(in: .gallery, to: galleryItems);
			snapshot.reloadSections([.header, .parameter, .description]);
			dataSource?.apply(snapshot);
		}
	}
	
	public func fetchItem() {
		if let itemId = itemId {
			client.itemController.item(forID: itemId).onMain { (item) in
				self.item = item;
			}.alertFailure(in: self);
		}
	}

	public func generateLayout() -> UICollectionViewLayout {
		return UICollectionViewCompositionalLayout { (index, _) -> NSCollectionLayoutSection? in
			switch self.sections[index] {
			case .header:
				return self.generateHeaderLayout();
			case .gallery:
				return self.generateGalleryLayout();
			case .parameter:
				return self.generateParameterLayout();
			case .description:
				return self.generateDescriptionLayout();
			}
		}
	}

	public func generateHeaderLayout() -> NSCollectionLayoutSection {
		let headerLayoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(128));
		let headerLayoutItem = NSCollectionLayoutItem(layoutSize: headerLayoutSize);
		let headerGroup = NSCollectionLayoutGroup.vertical(layoutSize: headerLayoutSize, subitem: headerLayoutItem, count: 1);
		headerGroup.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: .none, top: .fixed(8), trailing: .none, bottom: .fixed(8));
		let section = NSCollectionLayoutSection(group: headerGroup);
		return section;
	}
	
	public func generateGalleryLayout() -> NSCollectionLayoutSection {
		if hideGallery() {
			return generateEmptySection();
		}
		let galleryLayoutSize = NSCollectionLayoutSize(widthDimension: .absolute(200), heightDimension: .absolute(200));
		let galleryLayoutItem = NSCollectionLayoutItem(layoutSize: galleryLayoutSize);
		galleryLayoutItem.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4);
		let galleryGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(208));
		let galleryLayoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: galleryGroupSize, subitems: [galleryLayoutItem]);
		let section = NSCollectionLayoutSection(group: galleryLayoutGroup);
		return section;
	}
	
	public func generateParameterLayout() -> NSCollectionLayoutSection {
		let parameterLayoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.5), heightDimension: .estimated(68));
		let parameterLayoutItem = NSCollectionLayoutItem(layoutSize: parameterLayoutSize);
		let parameterGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(68));
		let parameterGroup = NSCollectionLayoutGroup.horizontal(layoutSize: parameterGroupSize, subitem: parameterLayoutItem, count: 2);
		let section = NSCollectionLayoutSection(group: parameterGroup);
		return section;
	}
	
	public func generateDescriptionLayout() -> NSCollectionLayoutSection {
		let descriptionLayoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(64));
		let descriptionLayoutItem = NSCollectionLayoutItem(layoutSize: descriptionLayoutSize);
		let descriptionGroup = NSCollectionLayoutGroup.horizontal(layoutSize: descriptionLayoutSize, subitem: descriptionLayoutItem, count: 1);
		let section = NSCollectionLayoutSection(group: descriptionGroup);
		return section;
	}
	
	public func hideGallery() -> Bool {
		if let item = item, !item.images.isEmpty {
			return false;
		} else {
			return true;
		}
	}
	
}

public class ItemHeaderCell: UICollectionViewCell {
	
	@IBOutlet weak var titleLabel: UILabel!
	
	@IBOutlet weak var creatorLabel: UILabel!
	
	@IBOutlet weak var creatorAvatar: AvatarView!
	
	@IBOutlet weak var toolbar: UIToolbar!
	
	public var item: Item? {
		didSet {
			load();
		}
	}
	
	public override func awakeFromNib() {
		toolbar.setBackgroundImage(UIImage(), forToolbarPosition: .any, barMetrics: .default);
		toolbar.setShadowImage(UIImage(), forToolbarPosition: .any);
	}
	
	public func load() {
		if let item = item {
			titleLabel.text = item.title;
			toolbar.items = [];
			setCreatorDefault();
			client.profileController.getProfile(forID: item.publisherID).onMain { (profile) in
				self.creatorAvatar.load(from: profile.id);
				self.creatorLabel.text = "By \(profile.username)";
			}
		} else {
			titleLabel.text = "";
			toolbar.items = [];
			setCreatorDefault();
		}
	}
	
	// Sets the views indicating the item's creator to a default state. Used both when there is no item and while the profile information is being loaded.
	public func setCreatorDefault() {
		creatorAvatar.loadDefault();
		creatorLabel.text = "By";
	}
	
}

public class ItemGalleryCell: UICollectionViewCell {
	
	@IBOutlet weak var imageView: UIImageView!
	
	public var imageID: ID? {
		didSet {
			load();
		}
	}
	
	public override func awakeFromNib() {
		contentView.clipsToBounds = true;
		contentView.layer.cornerRadius = 8.0;
	}
	
	public func load() {
		self.imageView.loadDefault();
		if let imageID = imageID {
			getImage(for: imageID).onMain { (image) in
				self.imageView.image = image;
			}
		}
	}
	
}

public enum ItemParameter {
	case style
	case category
}

public class ItemParameterCell: UICollectionViewCell {
	
	@IBOutlet weak var kindCell: UILabel!
	
	@IBOutlet weak var valueCell: UILabel!
	
	public var parameter: ItemParameter = .style {
		didSet {
			load();
		}
	}
	
	public var item: Item? {
		didSet {
			load();
		}
	}
	
	public func load() {
		switch parameter {
		case .style:
			kindCell.text = "Style";
		case .category:
			kindCell.text = "Category";
		}
		if let item = item {
			switch parameter {
			case .style:
				valueCell.text = item.style;
			case .category:
				valueCell.text = item.category.last ?? "";
			}
		} else {
			valueCell.text = "";
		}
	}
	
	
}

public class ItemDescriptionCell: UICollectionViewCell {
	
	@IBOutlet weak var textView: UITextView!
	
	public var item: Item? {
		didSet {
			load();
		}
	}

	public func load() {
		if let item = item {
			textView.text = item.description;
		} else {
			textView.text = "";
		}
	}
	
}
