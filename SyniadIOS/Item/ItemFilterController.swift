//
//  ItemFilterController.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 6/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit
import SyniadShared
import SyniadViewModel
import SyniadClient



public typealias ItemFilterDataSource = TableViewAdvancedDataSource<ItemFilterDisplaySection, ItemFilterDisplayItem>;

public typealias ItemFilterSnapshot = NSDiffableDataSourceSnapshot<ItemFilterDisplaySection, ItemFilterDisplayItem>;

public class ItemFilterController: UITableViewController {
	
	// MARK:- Variables

	
	public var dataSource: ItemFilterDataSource?;
	
	public var snapshot: ItemFilterSnapshot?;
	
	public var viewModel: FilterViewModel? {
		didSet {
			setup();
		}
	}
	
	public var cancellables = Set<AnyCancellable>();
	
	// MARK:- Initialisation
	
	public override func viewDidLoad() {
		dataSource = ItemFilterDataSource(tableView: tableView, cellProvider: { [unowned self] (tableView, index, item) -> UITableViewCell? in
			switch item {
			// The cells using bindings are only created once, so there is no need to manage cancelling.
			case .category:
				let row = tableView.dequeueReusableCell(withIdentifier: "treeSelector") as! CombineTableViewCell;
				row.textLabel?.text = viewModel?.categoryCellText
				viewModel?.selectedCategoryText.sink(receiveValue: { (text) in
					row.detailTextLabel?.text = text;
				}).store(in: &row.cancellables)
				return row;
			case .style:
				let row = tableView.dequeueReusableCell(withIdentifier: "treeSelector") as! CombineTableViewCell;
				row.textLabel?.text = viewModel?.styleCellText;
				viewModel?.selectedStyleText.sink(receiveValue: { (text) in
					row.detailTextLabel?.text = text;
				}).store(in: &row.cancellables);
				return row;
			case .tag(let tag):
				let cell = tableView.dequeueReusableCell(withIdentifier: "tagCell")!;
				cell.textLabel?.text = tag;
				return cell;
			case .tagMore:
				let cell = tableView.dequeueReusableCell(withIdentifier: "tagMoreCell");
				cell?.textLabel?.text = "More...";
				return cell;
			}
		});
		dataSource?.titleProvider = { [unowned self] (section) in
			guard let viewModel = viewModel else {
				return nil;
			}
			switch viewModel.sections[section] {
			case .main:
				return nil;
			case .tags:
				return "Tags";
			}
		}
		tableView.dataSource = dataSource;
		setup();
	}
	
	public func setup() {
		guard isViewLoaded, let viewModel = viewModel else {
			return;
		}
		
		snapshot = ItemFilterSnapshot();
		
		viewModel.$sections.sink { [unowned self] (sections) in
			snapshot?.appendSections(sections);
		}.store(in: &cancellables);
		
		viewModel.$mainItems.sink { [unowned self] (items) in
			guard snapshot != nil else {
				return;
			}
			snapshot!.clearSection(.main);
			snapshot!.appendItems(items, toSection: .main);
			dataSource?.apply(snapshot!);
		}.store(in: &cancellables);
		
		viewModel.tagDisplay.sink { [unowned self] (items) in
			guard snapshot != nil else {
				return;
			}
			snapshot!.clearSection(.tags);
			snapshot!.appendItems(items, toSection: .tags);
			dataSource?.apply(snapshot!);
		}.store(in: &cancellables);
		
	}
	
	public var treeSelectorMode: TreeMode?;
	
	public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let snapshot = snapshot else {
			return;
		}
		
		switch snapshot.sectionIdentifiers[indexPath.section] {
		case .main:
			switch snapshot.itemIdentifiers(inSection: .main)[indexPath.item] {
			case .category:
				treeSelectorMode = .category;
			case .style:
				treeSelectorMode = .style;
			default:
				break;
			}
			performSegue(withIdentifier: "showTreeSelector", sender: self);
		case .tags:
			if snapshot.itemIdentifiers(inSection: .tags)[indexPath.item] == .tagMore {
				performSegue(withIdentifier: "showTagSelector", sender: self);
			}
		}
		
	}
	
	public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let viewModel = viewModel else {
			return;
		}
		if let destination = segue.destination as? TreeViewController {
			switch treeSelectorMode {
			case .category:
				destination.viewModel = viewModel.categoryModel;
			case .style:
				destination.viewModel = viewModel.styleModel;
			case nil:
				break;
			}
		}
		if let destination = segue.destination as? TagSelectorController {
			destination.viewModel = viewModel.tagModel;
		}
	}
	
}
