//
//  ItemCell.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 28/05/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadShared
import SyniadClient
import SyniadViewModel

class ItemCell: UICollectionViewCell {
	
	// MARK:- Static
	
	public static let nib = UINib(nibName: "ItemCell", bundle: nil);
	
	public static let reuseIdentifier = "itemCell";
	
	// MARK:- Header
	
	@IBOutlet weak var explicitAuthorSourceView: UIView!
	
	/// The avatar for the publisher of the item.
	@IBOutlet weak var sourceAvatar: AvatarView!;
	
	/// The name of the publisher of the item.
	@IBOutlet weak var sourceName: UILabel!;
	
	@IBOutlet weak var explicitAuthorTitle: UILabel!
	
	/// The view displaying the title when the author is implicit.
	@IBOutlet weak var implicitAuthorTitle: UILabel!;
	
	/// An indicator of the item's popularity, typically some sort of number.
	@IBOutlet weak var popularityIndicator: UILabel!;
	
	@IBOutlet weak var contentImage: UIImageView!;
	
	/// The view displaying the user and action that caused the item to be shown to the current user.
	@IBOutlet weak var linkView: UIView?;
	
	/// The avatar of the user whose action caused the item to be shown to the current user; For instance, a followed person who shared it.
	@IBOutlet weak var linkAvatar: AvatarView?;
	
	/// The name of the user whose action caused the item to be shown to the current user. This is absent on the publisher's page, so it is optional.
	@IBOutlet weak var linkName: UILabel!;
	
	/// The kind of action that caused the item to be shown to the user, such as a share or like.
	@IBOutlet weak var linkKindDescription: UILabel!;
	
	/// The view containing the like button.
	@IBOutlet weak var likeView: UIView!;
	
	/// The button to like the item.
	@IBOutlet weak var likeButton: UIButton!;
	
	// MARK:- Visual Effect View
	
	@IBOutlet var visualEffectViews: [UIVisualEffectView]!
	
	public var viewModel: ItemCellViewModel? {
		didSet {
			setup();
		}
	}
	
	var cancellables = Set<AnyCancellable>();
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		contentView.layer.cornerRadius = 8;
		contentView.clipsToBounds = true;
		for visualEffectView in visualEffectViews {
			visualEffectView.layer.cornerRadius = 8;
			visualEffectView.clipsToBounds = true;
		}
		// TODO: Display link when link is set.
		
		linkView?.isHidden = true;
    }
	
	public override func prepareForReuse() {
		cancellables = Set<AnyCancellable>();
		explicitAuthorTitle.text = nil;
		implicitAuthorTitle.text = nil;
		sourceAvatar.image = nil;
		sourceName.text = nil;
		contentImage.image = nil;
		popularityIndicator.text = "";
	}
	
	public func setup() {
		guard let viewModel = viewModel else {
			return;
		}
		
		viewModel.displaysUser.onMain({ [unowned self] (displaysUser) in
			explicitAuthorSourceView.isHidden = !displaysUser;
			implicitAuthorTitle.isHidden = displaysUser;
		}, store: &cancellables);
		
		viewModel.title.sink(receiveValue: { [unowned self] (title) in
			explicitAuthorTitle.text = title;
			implicitAuthorTitle.text = title;
		}).store(in: &cancellables);
		viewModel.likeCount.onMain( { [unowned self] (count) in
			popularityIndicator.text = "\(count)";
		}, store: &cancellables);
		viewModel.imageID.onMain( { [unowned self] (id) in
			contentImage.load(from: id);
		}, store: &cancellables);
		
		viewModel.publisherUsername.onMain({ [unowned self] (username) in
			sourceName.text = username;
		}, store: &cancellables);
		viewModel.publisherAvatarID.onMain({ [unowned self] (id) in
			sourceAvatar.load(from: id);
		}, store: &cancellables);
		
	}
	
	/// Called when the like button is pressed.
	@IBAction func onLike(_ sender: Any) {
	}
	
}
