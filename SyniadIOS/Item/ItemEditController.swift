//
//  ItemEditController.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 7/07/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadShared
import SyniadClient
import SyniadViewModel
import Combine
import NIO

public enum EditTagItem: Hashable {
	case placeholder
	case tag(Tag)
}

public typealias EditTagDataSource = UICollectionViewDiffableDataSource<Int, EditTagItem>;

public typealias EditTagSnapshot = NSDiffableDataSourceSnapshot<Int, EditTagItem>;

class ItemEditController: UIViewController, UITextViewDelegate, UIDocumentPickerDelegate {
	
	private let descriptionPlaceholder = "Add a description...";
	
	// MARK: Views
	
	@IBOutlet weak var doneItem: UIBarButtonItem!
	
	@IBOutlet weak var titleField: UITextField!
	
	@IBOutlet weak var galleryView: UICollectionView!
	
	@IBOutlet weak var categoryLabel: UILabel!
	
	@IBOutlet weak var styleLabel: UILabel!
	
	@IBOutlet weak var descriptionView: UITextView!
	
	@IBOutlet weak var remainingCharactersLabel: UILabel!
	
	@IBOutlet weak var tagView: UICollectionView!;
	
	@IBOutlet weak var fileLabel: UILabel!
	
	@IBOutlet weak var fileRevertButton: UIButton!
	
	@IBOutlet weak var categoryRevertButton: UIButton!
	
	@IBOutlet weak var styleRevertButton: UIButton!
	
	// MARK:- Binders
	
	var galleryEditor: GalleryEditor?;
	
	var tagAdapter: TagCollectionAdapter?;
	
	// MARK: UI Delegates
	
	var descriptionViewDelegate: PlaceholderTextViewDelegate!;
	
	var cancellables = Set<AnyCancellable>();
	
	public var viewModel: ItemEditViewModel? {
		didSet {
			setup();
		}
	}

	// MARK: Initialisation.
	
    public override func viewDidLoad() {
        super.viewDidLoad()
		doneItem.isEnabled = false;
        // Do any additional setup after loading the view.
		fileRevertButton.isHidden = true;
		categoryRevertButton.isHidden = true;
		styleRevertButton.isHidden = true;
		descriptionViewDelegate = PlaceholderTextViewDelegate(displaying: "Add a Description Here", for: descriptionView);
		descriptionViewDelegate.onChange(onDescriptionUpdated(_:));
		descriptionView.delegate = descriptionViewDelegate;
		setup();
    }
	
	public func setup() {
		guard isViewLoaded, let viewModel = viewModel else {
			return;
		}
		
		titleField.text = viewModel.defaultTitle;

		galleryEditor = GalleryEditor(for: viewModel.galleryEditor);
		galleryEditor?.attach(galleryView, in: self);
		// The Adapter cannot accept the data directly due to a bug. In future, check if bug has been corrected and update. Alternatively, creating it as a separate view controller might help.
		viewModel.galleryEditor.$displayImages.sink(receiveValue: { (items) in
			self.galleryEditor?.images = items;
		}).store(in: &cancellables);
		
		viewModel.categoryText.assign(to: \.text, on: self.categoryLabel).store(in: &cancellables)
		viewModel.styleText.assign(to: \.text, on: self.styleLabel).store(in: &cancellables);
		
		descriptionView.text = viewModel.defaultDescription;
		viewModel.remainingDescriptionCharacters.sink { (remainingCharacters) in
			self.updateDescriptionLengthIndicator(remainingCharacters: remainingCharacters);
		}.store(in: &cancellables)
		
		viewModel.fileText.sink { (text) in
			self.fileLabel.text = text;
		}.store(in: &cancellables)
		
		tagAdapter = TagCollectionAdapter();
		tagAdapter!.attach(to: tagView);
		// Similar issue as above.
		viewModel.displayTags.sink { (tags) in
			self.tagAdapter?.tags = tags;
		}.store(in: &cancellables);
		
		viewModel.$validated.assign(to: \.isEnabled, on: doneItem).store(in: &cancellables);
		
		viewModel.cannotRevertCategory.assign(to: \.isHidden, on: categoryRevertButton).store(in: &cancellables);
		viewModel.cannotRevertStyle.assign(to: \.isHidden, on: styleRevertButton).store(in: &cancellables);
		viewModel.cannotRevertFile.assign(to: \.isHidden, on: fileRevertButton).store(in: &cancellables);
		
		viewModel.$validated.onMain({ (validated) in
			self.doneItem.isEnabled = validated;
		}, store: &cancellables);
	}
	
	// MARK: Text input updates
	
	@IBAction func onTitleChanged(_ sender: Any) {
		viewModel?.title = titleField.text;
	}
	
	public func onDescriptionUpdated(_ textView: UITextView) {
		viewModel?.description = textView.text;
	}
	
	// Updates the remaining characters
	func updateDescriptionLengthIndicator(remainingCharacters: Int) {
		var characterWord: String;
		if remainingCharacters.magnitude == 1 {
			characterWord = "Character";
		} else {
			characterWord = "Characters";
		}
		if remainingCharacters >= 0 {
			remainingCharactersLabel.textColor = .systemGreen;
			remainingCharactersLabel.text = "\(remainingCharacters) \(characterWord) Remaining";
		} else {
			remainingCharactersLabel.textColor = .systemRed;
			remainingCharactersLabel.text = "\(remainingCharacters) Excess \(characterWord)";
		}
	}
	
	public var treeMode: TreeMode?;
	
	// TODO: Use a search controller for this functionality.
	@IBAction func onSelectCategory(_ sender: Any) {
		treeMode = .category;
		performSegue(withIdentifier: "showTreeSelector", sender: self);
	}
	
	@IBAction func onSelectStyle(_ sender: Any) {
		treeMode = .style;
		performSegue(withIdentifier: "showTreeSelector", sender: self);
	}
	
	@IBAction func onEditTags(_ sender: Any) {
		performSegue(withIdentifier: "editTags", sender: self);
	}
	
	@IBAction func onSelectFile(_ sender: Any) {
		let picker = UIDocumentPickerViewController(documentTypes: ["com.luoja.pattern"], in: .import);
		picker.allowsMultipleSelection = false;
		picker.delegate = self;
		picker.shouldShowFileExtensions = false;
		present(picker, animated: true, completion: nil);
	}
	
	public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
		if let url = urls.first {
			viewModel?.fileName = url.lastPathComponent;
			viewModel?.fileData = try? Data(contentsOf: url);
		}
	}
	
	// MARK:- Segue Preparation
	
	public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let treeSelectorController = segue.destination as? TreeViewController, let treeMode = treeMode {
			switch treeMode {
			case .category:
				treeSelectorController.viewModel = viewModel?.categoryModel;
			case .style:
				treeSelectorController.viewModel = viewModel?.styleModel;
			}
		}
		if let tagSelectorController = segue.destination as? TagSelectorController {
			tagSelectorController.viewModel = viewModel?.tagModel;
		}
	}
	
	// MARK: Reverting content.
	
	@IBAction func onRevertFile(_ sender: Any) {
		viewModel?.fileName = nil;
		viewModel?.fileData = nil;
	}
	
	@IBAction func onRevertCategory(_ sender: Any) {
		viewModel?.category = nil
	}
	
	@IBAction func onStyleRevert(_ sender: Any) {
		viewModel?.style = nil;
	}
	
	
	@IBAction func onDone() {
		viewModel?.upload().onMain({ (_) in
			self.dismiss(animated: true, completion: nil);
		}).alertFailure(in: self);
	}
	
	@IBAction func onCancel() {
		self.dismiss(animated: true, completion: nil);
	}
	

}
