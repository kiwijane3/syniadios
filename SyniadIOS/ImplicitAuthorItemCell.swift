//
//  ImplicitAuthorItemCell.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 1/06/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadShared
import SyniadClient

class ImplicitAuthorItemCell: UICollectionViewCell {
    
	/// The view at the top of the view, containing the item title and popularity label.
	@IBOutlet weak var headerView: UIView!
	
	/// The label displaying the title.
	@IBOutlet weak var titleLabel: UILabel!
	
	/// The label displaying some sort of number representing quality.
	@IBOutlet weak var popularityLabel: UILabel!
	
	/// The view containing the like button
	@IBOutlet weak var likeView: UIView!
	
	/// The like button
	@IBOutlet weak var likeButton: UIButton!
	
	/// The main image view for displaying the item's images.
	@IBOutlet weak var mainImageView: UIImageView!
	
	// The item to be displayed.
	public var item: Item? {
		didSet {
			reload();
		}
	}
	
	override func awakeFromNib() {
		// TODO
		contentView.layer.cornerRadius = 16;
		contentView.clipsToBounds = true;
		headerView.layer.cornerRadius = 16;
		likeView.layer.cornerRadius = likeView.frame.width / 2;
	}
	
	public func reload() {
		if let item = item {
			titleLabel.text = item.title;
			// TODO: Maybe find some better way to display overall sentiment, taking into account negative reviews.
			popularityLabel.text = "\(item.likeCount)";
			if let firstImageId = item.images.first {
				do {
					try getClient().dataController.getData(withID: firstImageId).map({ (container) in
						return UIImage(data: container.data);
					}).onMain({ (image) in
						self.mainImageView.image = image;
					})
				} catch {
					print("Error loading image from id: \(firstImageId)");
				}
			}
		}
	}
	
}
