//
//  GalleryEditor.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 9/07/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit
import SyniadShared
import SyniadViewModel
import NIO
import Combine
import SyniadClient

public typealias GalleryDataSource = UICollectionViewDiffableDataSource<SingleSection, GalleryEditorDisplayItem>;

typealias GallerySnapshot = NSDiffableDataSourceSnapshot<SingleSection, GalleryEditorDisplayItem>;

// The gallery editor is used to add, remove, and reorder images attached to other elements, such as items and reviews.
public class GalleryEditor: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate, GalleryEditorPresenter {
	
	// MARK: Variables and initialisation
	
	// A reference to a view controller using this gallery editor. Used as a presentation context for image selection.
	public var viewController: UIViewController?;

	public var images: [GalleryEditorDisplayItem] = [] {
		didSet {
			var snapshot = GallerySnapshot();
			snapshot.appendSections([.only]);
			snapshot.appendItems(images, toSection: .only);
			self.dataSource?.apply(snapshot);
		}
	}
	
	var collectionView: UICollectionView?;
	
	var dataSource: GalleryDataSource?;
	
	var reorderingGesture: UILongPressGestureRecognizer?;
	
	var reordered: Bool = false;
	
	public var viewModel: GalleryEditorViewModel;
	
	var cancellables = Set<AnyCancellable>();
	
	public init(for viewModel: GalleryEditorViewModel) {
		self.viewModel = viewModel;
	}
	
	// MARK: Collection view setup and updating.
	
	public func attach(_ collectionView: UICollectionView, in controller: UIViewController) {
		self.viewController = controller;
		self.collectionView = collectionView;
		collectionView.register(GalleryEditorCell.nib, forCellWithReuseIdentifier: GalleryEditorCell.reuseIdentifier);
		collectionView.register(HeaderView.nib, forSupplementaryViewOfKind: HeaderView.supplementaryViewKind, withReuseIdentifier: HeaderView.reuseIdentifier);
		// Configure the gesture recogniser for reordering.
		dataSource = GalleryDataSource(collectionView: collectionView, cellProvider: { (collectionView, path, item) -> UICollectionViewCell? in
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryEditorCell.reuseIdentifier, for: path) as! GalleryEditorCell;
			switch item {
			case .image(let image):
				cell.galleryImage = image;
			case .placeholder:
				cell.galleryImage = nil;
			}
			return cell;
		});
		dataSource?.supplementaryViewProvider = { (collectionView: UICollectionView, kind: String, indexPath: IndexPath) -> UICollectionReusableView? in
			let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: HeaderView.supplementaryViewKind, withReuseIdentifier: HeaderView.reuseIdentifier, for: indexPath) as! HeaderView;
			headerView.title = "Images";
			// TODO: Check this.
			headerView.setBarButtonItems([
				UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
				UIBarButtonItem(image: UIImage(systemName: "plus"), style: .plain, target: self, action: #selector(self.addImage(sender:)))
			])
			return headerView;
		}
		collectionView.dataSource = dataSource;
		collectionView.setCollectionViewLayout(generateLayout(), animated: false);
		reorderingGesture = UILongPressGestureRecognizer(target: self, action: #selector(onLongDrag(_:)));
		reorderingGesture?.minimumPressDuration = 0.2;
		collectionView.addGestureRecognizer(reorderingGesture!);
	}
	
	public func present(items: [GalleryEditorDisplayItem]) {
		var snapshot = GallerySnapshot();
		snapshot.appendSections([.only]);
		snapshot.appendItems(items, toSection: .only);
		dataSource?.apply(snapshot, animatingDifferences: true, completion: nil);
	}
	
	public func generateLayout() -> UICollectionViewCompositionalLayout {
		let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(1));
		let item = NSCollectionLayoutItem(layoutSize: itemSize);
		item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4);
		let groupSize = NSCollectionLayoutSize(widthDimension: .absolute(200), heightDimension: .absolute(200));
		let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item]);
		let section = NSCollectionLayoutSection(group: group);
		section.orthogonalScrollingBehavior = .continuous;
		let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(44));
		let headerItem = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: HeaderView.supplementaryViewKind, alignment: .top);
		section.boundarySupplementaryItems = [headerItem];
		return UICollectionViewCompositionalLayout(section: section);
	}
	
	// MARK: User input handling
	
	@objc public func addImage(sender: UIButton) {
		if let viewController = viewController, UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
			let picker = UIImagePickerController();
			picker.sourceType = .photoLibrary;
			picker.mediaTypes = ["public.image"];
			picker.allowsEditing = true;
			picker.delegate = self;
			viewController.present(picker, animated: true, completion: nil);
		}
	}
	
	public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		if let uiImage = info[.editedImage] as? UIImage {
			viewModel.addImage(withData: uiImage.jpegData(compressionQuality: 0.5)!);
		}
		picker.dismiss(animated: true, completion: nil);
	}
	
	public var moveOrigin: Int?;

	@objc public func onLongDrag(_ gestureRecogniser: UIGestureRecognizer) {
		let location = gestureRecogniser.location(in: collectionView);
		switch gestureRecogniser.state {
		case .began:
			if let indexPath = collectionView?.indexPathForItem(at: location) {
				collectionView?.beginInteractiveMovementForItem(at: indexPath);
				moveOrigin = indexPath.item;
			}
		case .changed:
			collectionView?.updateInteractiveMovementTargetPosition(location);
			if let moveOrigin = moveOrigin, let currentIndex = collectionView?.indexPathForItem(at: location)?.item {
				// Temporarily display the result.
				viewModel.updateReordering(from: moveOrigin, to: currentIndex);
			} else {
				viewModel.resetReordering();
			}
		case .ended:
			if collectionView?.frame.contains(location) ?? false, let moveOrigin = moveOrigin,
			   let moveDestination = collectionView?.indexPathForItem(at: location)?.item {
				collectionView?.endInteractiveMovement();
				viewModel.finaliseReordering(from: moveOrigin, to: moveDestination);
			} else {
				viewModel.resetReordering()
			}
		case .cancelled:
			collectionView?.cancelInteractiveMovement();
			viewModel.resetReordering();
		default:
			break;
		}
	}

}
