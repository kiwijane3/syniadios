//
//  GalleryEditorCell.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 10/07/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadViewModel

public class GalleryEditorCell: UICollectionViewCell {

	public static var nib = UINib(nibName: "GalleryEditorCell", bundle: nil);
	
	public static var reuseIdentifier = "galleryEditorCell";
	
	var galleryImage: GalleryImage? {
		didSet {
			setup();
		}
	}
	
	let removeIcon = UIImage(systemName: "trash");
	
	let cancelRemoveIcon = UIImage(systemName: "trash.slash");
	
	@IBOutlet weak var imageView: UIImageView!
	
	@IBOutlet weak var removeIndicator: UIView!
	
	// The button for removing/cancelling
	@IBOutlet weak var toggleButton: UIButton!
	
	@IBOutlet weak var toggleButtonEffectView: UIVisualEffectView!;
	
	@IBOutlet weak var indicatorEffectView: UIVisualEffectView!;
	
	var cancellables = Set<AnyCancellable>();
	
	override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		// Round the corners of the view.
		contentView.layer.cornerRadius = 8;
		// Make the toggle button and indicator round.
		// Make the removal indicator hidden by default.
		removeIndicator.isHidden = true;
		toggleButtonEffectView.layer.cornerRadius = toggleButtonEffectView.frame.height / 2;
		toggleButtonEffectView.clipsToBounds = true;
		indicatorEffectView.layer.cornerRadius = indicatorEffectView.frame.height / 2;
		toggleButtonEffectView.clipsToBounds = true;
    }
	
	public func setup() {
		guard let galleryImage = galleryImage else {
			return;
		}
		galleryImage.$action.sink { (action) in
			switch action {
			case .add, .none:
				self.toggleButton.setImage(self.removeIcon, for: .normal);
				self.removeIndicator.isHidden = true;
			case .remove:
				self.toggleButton.setImage(self.cancelRemoveIcon, for: .normal);
				self.removeIndicator.isHidden = false;
			}
		}.store(in: &cancellables);
		galleryImage.$imageData.sink { (data) in
			guard let data = data else {
				return;
			}
			beginAsync().map { (_) -> (UIImage?) in
				return UIImage(data: data);
			}.onMain { (image) in
				self.imageView.image = image;
			}
		}.store(in: &cancellables);
	}
	
	@IBAction func onToggle(_ sender: Any) {
		self.galleryImage?.toggleRemove();
	}
	
}

public protocol GalleryEditorCellDelegate {
	
	func cancelUpload(for galleryImage: GalleryImage);
	
}
