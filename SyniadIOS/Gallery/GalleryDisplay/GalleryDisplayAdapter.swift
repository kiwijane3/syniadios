//
//  GalleryDisplayAdapter.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 15/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import SyniadShared
import UIKit

public typealias GalleryDisplayDataSource = UICollectionViewDiffableDataSource<Int, ID>;
public typealias GalleryDisplaySnapshot = NSDiffableDataSourceSnapshot<Int, ID>

public class GalleryDisplayAdapter {
	
	public var images: [ID] = [] {
		didSet {
			load();
		}
	}
	
	public var dataSource: GalleryDisplayDataSource?;
	
	public var snapshot: GalleryDisplaySnapshot?;
	
	public var heightConstraint: NSLayoutConstraint?;
	
	public func attach(to collectionView: UICollectionView) {
		collectionView.register(GalleryDisplayCell.nib, forCellWithReuseIdentifier: GalleryDisplayCell.reuseIdentifier);
		dataSource = GalleryDisplayDataSource(collectionView: collectionView, cellProvider: { (collectionView, indexPath, imageID) -> UICollectionViewCell? in
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryDisplayCell.reuseIdentifier, for: indexPath) as! GalleryDisplayCell;
			cell.image.load(from: imageID);
			return cell;
		});
		collectionView.dataSource = dataSource;
		collectionView.setCollectionViewLayout(generateLayout(), animated: false);
		// Setup the collectionView to have a size calculated to fit the content.
		heightConstraint = collectionView.heightAnchor.constraint(equalToConstant: 0);
		collectionView.superview?.addConstraint(heightConstraint!);
		load();
	}
	
	public func load() {
		guard let dataSource = dataSource else {
			return
		}
		var snapshot = GalleryDisplaySnapshot();
		snapshot.appendSections([0]);
		snapshot.appendItems(images, toSection: 0);
		dataSource.apply(snapshot);
		// Set the height of the collectionView to an appropriate height.
		if images.isEmpty {
			heightConstraint?.constant = 0;
		} else {
			heightConstraint?.constant = 200;
		}
	}
	
	public func generateLayout() -> UICollectionViewCompositionalLayout {
		return UICollectionViewCompositionalLayout { (_, _) -> NSCollectionLayoutSection? in
			if self.images.isEmpty {
				return generateEmptySection();
			} else {
				let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1));
				let item = NSCollectionLayoutItem(layoutSize: itemSize);
				item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4);
				let groupSize =  NSCollectionLayoutSize(widthDimension: .absolute(200), heightDimension: .absolute(200));
				let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item]);
				let section = NSCollectionLayoutSection(group: group);
				section.orthogonalScrollingBehavior = .continuous;
				return section;
			}
		}
	}
	
}

public class GalleryDisplayCell: UICollectionViewCell {
	
	public static let nib = UINib(nibName: "ImageDisplayCell", bundle: nil);
	
	public static let reuseIdentifier = "galleryDisplayCell";
	
	@IBOutlet weak var image: UIImageView!
	
	public override func awakeFromNib() {
		self.contentView.layer.cornerRadius = 8;
		self.contentView.clipsToBounds = true;
	}
	
}
