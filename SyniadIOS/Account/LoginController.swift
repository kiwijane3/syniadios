//
//  FirstViewController.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 20/02/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadClient
import SyniadViewModel
import NIO

class LoginController: UIViewController {
	
	@IBOutlet weak var emailField: UITextField!
	
	@IBOutlet weak var passwordField: UITextField!
	
	@IBOutlet weak var loginButton: UIButton!
	
	
	private var validated: Bool = false {
		didSet {
			loginButton.isEnabled = validated;
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Add a target to validate the input when the credentials are edited.
		emailField.addTarget(self, action: #selector(onEdit), for: .allEditingEvents);
		passwordField.addTarget(self, action: #selector(onEdit), for: .allEditingEvents);
	}

	@objc func onEdit() {
		if let email = emailField.text, email != "", validateAsEmail(email), let password = passwordField.text, password.count > 8 {
			validated = true;
		} else {
			validated = false;
		}
	}
	
	@IBAction func onLogin(_ sender: Any) {
		if validated, let email = emailField.text, let password = passwordField.text {
			client.accountController.login(withEmail: email, password: password).onMain ({ (loginInfo) in
				storeLoginInfo(loginInfo);
				let delegate = UIApplication.shared.delegate as! AppDelegate;
				delegate.loadMain();
			}).whenFailure { error in
				errorAlert(for: error, in: self);
			}
		}
	}
	
}

