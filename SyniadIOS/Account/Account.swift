//
//  Account.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 20/02/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import SyniadShared
import KeychainAccess

let userJwtKey = "syniad-user-jwt";
let userProfileKey = "syniad-user-profile";

func storeLoginInfo(_ loginInfo: LoginInfo) {
	keychain()["syniad-token"] = loginInfo.token;
	keychain()["syniad-profile"] = loginInfo.profile;
}

func retrieveLoginInfo() -> LoginInfo? {
	if let token = keychain()["syniad-token"], let profile = keychain()["syniad-profile"] {
		return LoginInfo(profile: profile, token: token);
	} else {
		return nil;
	}
}

func keychain() -> Keychain {
	return Keychain();
}
