//
//  SecondViewController.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 20/02/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import SyniadViewModel

class SignupController: UIViewController {

	@IBOutlet weak var emailField: UITextField!
	
	@IBOutlet weak var usernameField: UITextField!
	
	@IBOutlet weak var passwordField: UITextField!
	
	@IBOutlet weak var signupButton: UIButton!
	
	private var validated: Bool = false {
		didSet {
			signupButton.isEnabled = validated;
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Call onEdit to validate input when an edit occurs.
		emailField.addTarget(self, action: #selector(onEdit), for: .allEditingEvents);
		usernameField.addTarget(self, action: #selector(onEdit), for: .allEditingEvents);
		passwordField.addTarget(self, action: #selector(onEdit), for: .allEditingEvents);
	}

	
	@IBAction func onSignup(_ sender: Any) {
		if validated, let email = emailField.text, let username = usernameField.text, let password = passwordField.text {
			client.accountController.createAccount(withEmail: email, username: username, password: password).onMain { (loginInfo) -> (Void) in
				storeLoginInfo(loginInfo);
				let delegate = UIApplication.shared.delegate as! AppDelegate;
				delegate.loadMain();
			}.whenFailure({ (error) in
				errorAlert(for: error, in: self)
			});
		}
	}
	
	
	@objc func onEdit() {
		if let email = emailField.text, email != "", validateAsEmail(email), let username = usernameField.text, username != "", let password = passwordField.text, password.count > 7 {
			validated = true;
		} else {
			validated = false;
		}
	}
	
}

