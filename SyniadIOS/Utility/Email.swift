//
//  Email.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 20/02/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation

public func validateAsEmail(_ candidate: String) -> Bool {
	// This method evaluates the candidate against a regex from stack overflow. It would be a good idea to test this regex.
	return candidate.range(of: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}", options: .regularExpression) != nil;
}
