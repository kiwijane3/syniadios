//
//  ImageLoader.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 15/09/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import SyniadShared
import SyniadClient
import NIO
import UIKit

public var imageCache = NSCache<NSString, UIImage>();

/** public func getImage(for id: ID) -> EventLoopFuture<UIImage?> {
	if let uiImage = imageCache.object(forKey: NSString(string: id)) {
		return client.async().result(uiImage);
	} else {
		return client.dataController.getData(withID: id).map { (container) -> (UIImage?) in
			if container.dataType.isImage, let uiImage = UIImage(data: container.data) {
				imageCache.setObject(uiImage, forKey: NSString(string: id));
				return uiImage;
			} else {
				return nil;
			}
		}
	}
} */

public class ImageLoader {
	
	public var client: SyniadClient;
	
	public var cache: NSCache<NSString, UIImage>;
	
	public var promises: [String: EventLoopPromise<UIImage?>];
	
	public init(using client: SyniadClient) {
		self.client = client;
		self.cache = NSCache();
		self.promises = [:];
	}
	
	public func getImage(for id: ID) -> EventLoopFuture<UIImage?> {
		if let image = cache.object(forKey: NSString(string: id)) {
			return client.async().result(image);
		} else if let imagePromise = promises[id] {
			return imagePromise.futureResult;
		} else {
			return fetchImage(for: id);
		}
	}
	
	public func fetchImage(for id: ID) -> EventLoopFuture<UIImage?> {
		let imagePromise = client.eventLoop.makePromise(of: UIImage?.self);
		promises[id] = imagePromise;
		client.dataController.getData(withID: id).map { (container) -> (UIImage?) in
			if container.dataType.isImage {
				return UIImage(data: container.data);
			} else {
				return nil;
			}
		}.transparently { (image) in
			if let image = image {
				self.cache.setObject(image, forKey: NSString(string: id));
			}
		}.cascade(to: imagePromise);
		return imagePromise.futureResult;
	}
	
}
