//
//  Publisher.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 22/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import Combine

public extension Publisher {
	
	public func onMain(_ action: @escaping (Output) -> Void, store: inout Set<AnyCancellable>) {
		self.receive(on: RunLoop.main).sink { (_) in
			return;
		} receiveValue: { (output) in
			action(output);
		}.store(in: &store);
	}
	
}
