//
//  Alert.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 22/02/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit
import NIO
import SyniadViewModel

// Code for handling errors of various kinds.

public func errorAlert(for error: Error, in controller: UIViewController?) {
	debugPrint("Encountered error: \(error)");
	guard let controller = controller else {
		return;
	}
	if error is NIOConnectionError {
		displayConnectionErrorAlert(in: controller);
	} else {
		displayGenericErrorAlert(in: controller);
	}
}

public func errorAlert(for errors: [Error], in controller: UIViewController) {
	if errors.contains(where: { (error) -> Bool in
		return error is NIOConnectionError;
	}) {
		displayConnectionErrorAlert(in: controller);
	} else {
		displayGenericErrorAlert(in: controller);
	}
}

public func displayConnectionErrorAlert(in controller: UIViewController) {
	// TODO: The current error message is for development. Production versions shouldn't direct users to check the server.
	let alert = UIAlertController(title: "Connection Failed", message: "Could not connect to the server. Ensure this device is connected to the internet and a syniad server is available at app.luoja.co", preferredStyle: .alert);
	alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil));
	DispatchQueue.main.async {
		controller.present(alert, animated: true, completion: nil);
	}
}

public func displayGenericErrorAlert(in controller: UIViewController) {
	// TODO: The current error message is for development. Production versions shouldn't direct users to check debug logs they probably can't access anyway.
	let alert = UIAlertController(title: "Could Not Complete", message: "Sorry, the application encountered an error and could not complete the requested action. Check the debug logs for more information.", preferredStyle: .alert);
	alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil));
	DispatchQueue.main.async {
		controller.present(alert, animated: true, completion: nil);
	}
}

public func displayTextInputAlert(in controller: UIViewController, title: String?, placeholder: String?) -> EventLoopFuture<String?> {
	let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert);
	alert.addTextField { (textField) in
		textField.placeholder = placeholder;
	}
	let inputPromise = SyniadViewModel.client.eventLoop.makePromise(of: Optional<String>.self);
	alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
		inputPromise.succeed(nil);
	}));
	alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { (_) in
		inputPromise.succeed(alert.textFields?.first?.text);
	}));
	controller.present(alert, animated: true, completion: nil);
	return  inputPromise.futureResult;
}

public extension EventLoopFuture {
	
	public func alertFailure(in controller: UIViewController?) {
		self.whenFailure { (error) in
			errorAlert(for: error, in: controller);
		}
	}
	
}
