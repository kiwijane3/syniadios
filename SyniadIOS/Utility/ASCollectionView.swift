//
//  ASCollectionView.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 13/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit

// A handy subclass of UICollectionView that has an intrinsicSize of its contentSize. Useful for static layouts integrating dynamic data.
class ASCollectionView: UICollectionView {

}
