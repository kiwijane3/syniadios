//
//  SubscriptionManager.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 20/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import Combine

public class BindingManager {
	
	private var store: [(object: AnyObject, bindings: Set<AnyCancellable>)] = [];
	
	public func bindings(for object: AnyObject) -> Set<AnyCancellable> {
		var member = store.first { (member) -> Bool in
			return member.object === object;
		};
		if member == nil {
			member = (object: object, bindings: Set<AnyCancellable>());
			store.append(member!);
		}
		return member!.bindings;
	}
	
	public func cancel(for object: AnyObject) {
		var member = store.first { (member) -> Bool in
			return member.object === object;
		}
		member?.bindings.removeAll();
	}
	
}
