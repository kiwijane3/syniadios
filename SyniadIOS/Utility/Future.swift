//
//  Future.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 26/02/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit
import NIO

public extension EventLoopFuture {
	
	func onMain(_ callback: @escaping (Value) -> Void) -> EventLoopFuture<Void> {
		return self.map { (result) -> Void in
			DispatchQueue.main.async {
				callback(result);
			}
		}
	}
	
	func catchOnMain(_ callback: @escaping (Error) -> Void) {
		self.whenFailure { (error) in
			DispatchQueue.main.async {
				callback(error);
			}
		}
	}
	
}

func onMain(_ callback: @escaping () -> Void) {
	DispatchQueue.main.async {
		callback();
	}
}

public extension EventLoopFuture {
	
	/// Transforms a future to result in a value that does not depend on the outcome of the result.
	func result<T>(_ value: T) -> EventLoopFuture<T> {
		return self.map { (_) -> (T) in
			return value;
		}
	}
	
	/// Transforms a future by doing an asynchronous action that depends on this future's value and propagates that value. It is called transparently because it does not change the output, similar to how an optically transparent material does not change the image seen through it.
	func transparently<T>(_ action: @escaping (Value) -> EventLoopFuture<T> ) -> EventLoopFuture<Value> {
		return self.flatMap { (value) -> EventLoopFuture<Value> in
			return action(value).result(value);
		}
	}
	
	func transparently<T>(_ action: @escaping (Value) -> T) -> EventLoopFuture<Value> {
		return self.map { (value) -> Value in
			action(value);
			return value;
		}
	}
	
	/// Transforms a future doing an asynchronous that depends on this future's value if a predicate, also depending on this future's value, returns true. Similar to transparently(_:), but conditional.
	func transparently<T>(if predicate: @escaping (Value) -> Bool, _ action: @escaping (Value) -> EventLoopFuture<T>) -> EventLoopFuture<Value> {
		return self.flatMap { (value) -> EventLoopFuture<Value> in
			if predicate(value) {
				return action(value).result(value);
			} else {
				return self.eventLoop.makeSucceededFuture(value);
			}
		}
	}
	
	func transparently<T>(if condition: Bool, _ action: @escaping (Value) -> EventLoopFuture<T>) -> EventLoopFuture<Value> {
		if condition {
			return self.flatMap { (value) -> EventLoopFuture<Value> in
				return action(value).result(value);
			}
		} else {
			return self;
		}
	}
	
	func execute(if condition: Bool, _ action: @escaping () -> EventLoopFuture<Void>) -> EventLoopFuture<Void> {
		if condition {
			return self.flatMap { (_) -> EventLoopFuture<Void> in
				return action();
			}
		} else {
			return self.result(Void());
		}
	}
	
	func flatMap<T>(if predicate: @escaping (Value) -> Bool, _ action: @escaping (Value) -> EventLoopFuture<T?>) -> EventLoopFuture<T?> {
		return self.flatMap { (value) -> EventLoopFuture<T?> in
			if predicate(value) {
				return action(value)
			} else {
				return self.eventLoop.makeSucceededFuture(nil);
			}
		}
	}
	
	func flatMap<T>(if condition: Bool, _ action: @escaping (Value) -> EventLoopFuture<T?>) -> EventLoopFuture<T?> {
		if condition {
			return self.flatMap { (value) -> EventLoopFuture<T?> in
				return action(value);
			}
		} else {
			return eventLoop.makeSucceededFuture(nil);
		}
	}
	
	func map<T>(if condition: Bool, _ action: @escaping (Value) -> T) -> EventLoopFuture<T?> {
		if condition {
			return self.map { value -> T in
				return action(value);
			}
		} else {
			return eventLoop.makeSucceededFuture(nil);
		}
	}
	
	func map<T>(if predicate: @escaping (Value) -> Bool, _ action: @escaping (Value) -> T) -> EventLoopFuture<T?> {
		return self.map { (value) -> T? in
			if predicate(value) {
				return action(value);
			} else {
				return nil;
			}
		}
	}
	
	func ifLet<T>(_ value: T?, _ action: @escaping (T) -> EventLoopFuture<Void>) -> EventLoopFuture<Void> {
		if let value = value {
			return self.flatMap { _ -> EventLoopFuture<Void> in
				return action(value);
			}
		} else {
			return self.result(Void());
		}
	}
	
	/// Transforms a future to include a validation check using the result that does not change the result.
	func validate(_ action: @escaping (Value) throws -> Void) -> EventLoopFuture<Value> {
		self.flatMapThrowing { (value) -> Value in
			try action(value);
			return value;
		}
	}
	
	func end() {
		// Does nothing, prevents unused result nonsense
	}
	
}
