//
//  Collection.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 16/09/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit

func generateEmptySection() -> NSCollectionLayoutSection {
	let emptySize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(1));
	let emptyItem = NSCollectionLayoutItem(layoutSize: emptySize);
	let emptyGroup = NSCollectionLayoutGroup.vertical(layoutSize: emptySize, subitem: emptyItem, count: 1);
	let emptySection = NSCollectionLayoutSection(group: emptyGroup);
	return emptySection;
}
