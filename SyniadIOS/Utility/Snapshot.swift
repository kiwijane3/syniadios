//
//  Snapshot.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 16/09/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit

extension NSDiffableDataSourceSnapshot {
	
	public mutating func clearSections(_ sections: [SectionIdentifierType]) {
		for section in sections {
			clearSection(section);
		}
	}
	
	public mutating func clearSection(_ section: SectionIdentifierType) {
		deleteItems(self.itemIdentifiers(inSection: section));
	}
	
	public func isEmpty(_ section: SectionIdentifierType) -> Bool {
		return numberOfItems(inSection: section) > 0;
	}
	
}
