//
//  TitleDataSource.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 11/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit

public class TableViewAdvancedDataSource<S: Hashable, I: Hashable>: UITableViewDiffableDataSource<S, I> {
	
	public var titleProvider: ((Int) -> String?)?;
	
	public override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return titleProvider?(section);
	}
	
}
