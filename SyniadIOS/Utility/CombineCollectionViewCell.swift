//
//  CombineCollectionViewCell.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 20/11/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import UIKit
import Combine

@IBDesignable
class CombineCollectionViewCell: UICollectionViewCell {

	public var cancellables = Set<AnyCancellable>();
	
	public override func prepareForReuse() {
		super.prepareForReuse();
		cancellables = Set<AnyCancellable>();
	}
	
}

