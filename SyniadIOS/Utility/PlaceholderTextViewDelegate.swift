//
//  PlaceholderTextViewDelegate.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 24/07/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit

public class PlaceholderTextViewDelegate: NSObject, UITextViewDelegate {
	
	// The text to display as a placeholder;
	public var placeholderText: String;
	
	public var textView: UITextView;
	
	// A handler to be called when text is changed.
	private var changeHandler: ((UITextView) -> Void)?;
	
	// The text of the text view.
	public var text: String? {
		get {
			return textView.text;
		}
		set {
			textView.text = newValue;
			setPlaceholder(textView);
		}
	}
	
	public init(displaying text: String, for textView: UITextView) {
		self.placeholderText = text;
		self.textView = textView;
		super.init();
		textView.delegate = self;
		setPlaceholder(textView);
	}
	
	public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
		if textView.textColor == .placeholderText {
			textView.text = "";
			textView.textColor = .label;
		}
		// Always return true since we are just editing the placeholder;
		return true;
	}
	
	public func textViewDidEndEditing(_ textView: UITextView) {
		setPlaceholder(textView);
	}
	
	public func setPlaceholder(_ textView: UITextView) {
		if textView.text.isEmpty || textView.text == nil {
			textView.textColor = .placeholderText;
			textView.text = placeholderText;
		} else {
			textView.textColor = .label;
		}
	}

	public func textViewDidChange(_ textView: UITextView) {
		changeHandler?(textView);
	}

	public func onChange(_ handler: @escaping (UITextView) -> Void) {
		changeHandler = handler;
	}
	
}
